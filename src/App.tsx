import React from "react";
import Router from "./Route";
import GlobalStyles from "./ui/GlobalStyles";

export default function App() {
  return (
    <>
      <GlobalStyles />
      <Router />
    </>
  );
}
