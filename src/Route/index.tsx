import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import HomeContainer from "../pages/Home/HomeContainer";
import Header from "../ui/Header";

export default () => (
  <Router>
    <Header />
    <Switch>
      <Route path="/" exact component={HomeContainer} />
      <Redirect from="*" to="/" />
    </Switch>
  </Router>
);
