import React from "react";
import { useQuery, gql } from "@apollo/client";

const GET_CONTINENTS = gql`
  query {
    books {
      title
      author
    }
  }
`;

export default function Continents() {
  const { loading, data } = useQuery(GET_CONTINENTS);
  console.log(data);
  if (loading) return <div>loading....</div>;
  return <>{/* <ul>{data && <div>{data.ping}</div>}</ul> */}</>;
}
