import React, { useEffect } from "react";
import { KakaoMapProps } from "../types";

declare global {
  interface Window {
    kakao: any;
  }
}

const { kakao } = window;
let map;

export default function KakaoMap({
  lat,
  lng,
  restaurants,
  handelMarkerClick,
}: KakaoMapProps) {
  const createMap = (mapContainer: HTMLElement | null) => {
    const options = {
      center: new kakao.maps.LatLng(lng, lat),
      level: 3,
    };

    map = new kakao.maps.Map(mapContainer, options);
  };

  const displayMarkers = () => {
    restaurants.forEach((restaurant) => {
      const marker = new kakao.maps.Marker({
        map,
        position: new kakao.maps.LatLng(
          parseFloat(restaurant.y),
          parseFloat(restaurant.x)
        ),
      });

      kakao.maps.event.addListener(
        marker,
        "click",
        handelMarkerClick(restaurant.id)
      );
    });
  };

  useEffect(() => {
    const mapContainer = document.getElementById("map");

    createMap(mapContainer);
  }, []);

  useEffect(() => {
    displayMarkers();
  }, [restaurants]);

  return <div id="map" style={{ width: "100vw", height: "100vh" }} />;
}
