import React, { useEffect, useState } from "react";
import { getAdrress, getRestaurant } from "../../utils/api";
import { KakaoAdrress, KakaoPlaceData } from "../../types";
import KakaoMap from "../../components/KakaoMap";
import { AxiosResponse } from "axios";

export default function HomeContainer() {
  const [lat, setLat] = useState<number>(0);
  const [lng, setLng] = useState<number>(0);
  const [restaurants, setRestaurants] = useState<KakaoPlaceData[]>([]);

  const handelMarkerClick = (id: string) => () => {
    alert(id);
  };

  useEffect(() => {
    getData();

    async function getData(): Promise<void> {
      await fetchAdrress("알서포트");

      if (lat && lng) await fetchRestaurant();
    }

    async function fetchAdrress(str: string): Promise<void> {
      const {
        data: { documents },
      } = await getAdrress(str);
      const result: KakaoAdrress = documents[0];

      setLat(parseFloat(result.x));
      setLng(parseFloat(result.y));
    }

    async function fetchRestaurant(): Promise<void> {
      const array: Promise<AxiosResponse<any>>[] = [];

      for (let i = 1; i <= 45; i++) {
        array.push(getRestaurant(lat, lng, i));
      }
      const promiseResult = await Promise.all(array);
      const restauranteResult = promiseResult
        .map((el: any) => el.data.documents)
        .flat();

      setRestaurants(restauranteResult);
    }
  }, [lat, lng]);

  return (
    <>
      {restaurants.length && lng && lat && (
        <KakaoMap
          lat={lat}
          lng={lng}
          restaurants={restaurants}
          handelMarkerClick={handelMarkerClick}
        />
      )}
    </>
  );
}
