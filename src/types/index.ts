export interface Header {
  Authorization: string;
}

export interface KakaoAdrress {
  address_name: string;
  category_group_code: string;
  category_group_name: string;
  category_name: string;
  distance: string;
  id: string;
  phone: string;
  place_name: string;
  place_url: string;
  road_address_name: string;
  x: string;
  y: string;
}

export interface KakaoPlaceData {
  length: number;
  address_name: string;
  category_group_code: string;
  category_group_name: string;
  category_name: string;
  distance: string;
  id: string;
  phone: string;
  place_name: string;
  place_url: string;
  road_address_name: string;
  x: string;
  y: string;
}

export interface KakaoMapProps {
  lat: number;
  lng: number;
  restaurants: KakaoPlaceData[];
  handelMarkerClick: (id: string) => void;

  // children: ?React.ReactNode;
}

export interface HeaderStyleProps {
  current: boolean;
}
