import React from "react";
import { Link, withRouter } from "react-router-dom";
import styled from "styled-components";
import { HeaderStyleProps } from "../types";

export default withRouter(function Header({ location: { pathname } }) {
  return (
    <HeaderContainer>
      <List>
        <Title>
          <SLink to="/">
            <div>Title</div>
          </SLink>
        </Title>
        <Item current={pathname === "/"}>
          <SLink to="/">
            <div>1</div>
          </SLink>
        </Item>
        <Item current={pathname === "/rank"}>
          <SLink to="/search">
            <div>2</div>
          </SLink>
        </Item>
      </List>
    </HeaderContainer>
  );
});

const HeaderContainer = styled.header`
  color: #000000;
  width: 100%;
  height: 4rem;
  display: flex;
  align-items: center;
  background-color: #ffffff;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10;
`;

const List = styled.ul`
  display: flex;
`;

const Title = styled.div`
  width: 9rem;
  height: 4rem;
  text-align: center;
  font-size: 2rem;
`;

const Item = styled.li`
  width: 5rem;
  height: 4rem;
  text-align: center;
  border-bottom: 3px solid
    ${(props: HeaderStyleProps) =>
      props.current ? "rgb(70, 119, 255)" : "transparent"};
  transition: border-bottom 0.5s ease-in-out;
`;

const SLink = styled(Link)`
  height: 4rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;
