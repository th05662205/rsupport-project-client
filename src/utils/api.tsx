import axios from "axios";
import { Header } from "../types";

const headers: Header = {
  Authorization: `KakaoAK ${process.env.KAKAO_REST_API_KEY}`,
};

export const getAdrress = async (str: string) =>
  await axios.get(
    `https://dapi.kakao.com/v2/local/search/keyword.json?query=${str}&sort=accuracy`,
    { headers }
  );

export const getRestaurant = async (x: number, y: number, pageCount: number) =>
  await axios.get(
    `https://dapi.kakao.com/v2/local/search/category.json?category_group_code=FD6&x=${x}&y=${y}&radius=3000&sort=accuracy&page=${pageCount}`,
    { headers }
  );
